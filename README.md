vagrant-centos-box
================

Build a basic Vagrant box set for Development with Maven, thift, lein, and rbenv installed.

This can be used as a simplified template to installing other boxes.  Though this box is a centos box, 
it could easily be changed to use another base image.

This uses puppet to do the provisioning of the box

## Installation

* If you haven't already, go over to [http://www.vagrantup.com/](http://www.vagrantup.com/) and follow the installation instructions
* Add the box `precise64` via `vagrant box add precise64 http://files.vagrantup.com/precise64.box`
* Add the box `precise64` via `vagrant box add CentOS-6.4-x86_64-v20130731 http://developer.nrel.gov/downloads/vagrant-boxes/CentOS-6.4-x86_64-v20130731.box`
* Clone this repo `git clone git@bitbucket.org:jstapleton/vagrant-centos-dev.git`
* All done; `vagrant up`

## Customization
Adding your own sync'd folders.  This is easiest done by editing: ~/.vagrant.d/Vagrantfile

```
Vagrant.configure("2") do |config|
  config.vm.synced_folder "~/workspaces/myproject", "/home/vagrant/myproject"
  config.vm.synced_folder "~/.m2", "/home/vagrant/.m2"
end
```

## Usage

Feel free to customise further or use as is.
Jump onto the VM by `vagrant ssh`.