# pretty common build tools
package { 'java-1.7.0-openjdk-devel':
  ensure  => installed,
}
package { 'git':
  ensure  => installed,
}
package { 'rpm-build':
  ensure => installed,
}
package { 'redhat-rpm-config':
  ensure => installed,
}
package { 'make':
  ensure => installed,
}
package { 'gcc':
  ensure => installed,
}
package { 'man':
  ensure => installed,
}
package { 'tree':
  ensure => installed,
}
package { 'vim-minimal':
  ensure => installed,
}
exec { 'maven':
      	command  => 'wget -O /opt/apache-maven-3.1.1-bin.tar.gz http://apache.spinellicreations.com/maven/maven-3/3.1.1/binaries/apache-maven-3.1.1-bin.tar.gz &&
      	                 tar -C /opt -zxf /opt/apache-maven-3.1.1-bin.tar.gz &&
      	                 rm -rf /opt/apache-maven-3.1.1-bin.tar.gz',
      	unless   => '[ -d /opt/apache-maven-3.1.1 ]',
      	provider => 'shell',
}

file { '/etc/profile.d/mvn.sh':
       ensure => present,
       content => "pathmunge /opt/apache-maven-3.1.1/bin",
       require => Exec['maven'],
}

# For thrift compiler build
package { 'python-devel':
  ensure => installed,
}
package { 'ruby-devel':
  ensure => installed,
}
package { 'automake':
  ensure => installed,
}
package { 'libtool':
  ensure => installed,
}
package { 'pkgconfig':
  ensure => installed,
}
package { 'gcc-c++':
  ensure => installed,
}
package { 'boost-devel':
  ensure => installed,
}
package { 'libevent-devel':
  ensure => installed,
}
package { 'zlib-devel':
  ensure => installed,
}
# For clojure development
exec { 'lein':
      	command  => 'mkdir -p /usr/local/bin && wget -O /usr/local/bin/lein https://raw.github.com/technomancy/leiningen/stable/bin/lein && chmod oug+rx /usr/local/bin/lein',
      	unless   => '[ -f /usr/local/bin/lein ]',
      	provider => 'shell',
}

### This downloads a pre-compiled copy of thrift compiled on the VM that this puppet script came with.  This way everyone doesn't have to compile it themselves.
### Origial source URL: http://archive.apache.org/dist/thrift/0.6.1/thrift-0.6.1.tar.gz
exec { 'thrift':
      	command  => 'wget -O /tmp/thrift-0.6.1-bin-centos6.tar.gz https://www.dropbox.com/s/gr9ly86qd9n9g2r/thrift-0.6.1-bin-centos6.tar.gz &&
      	             tar -C /tmp -zxf /tmp/thrift-0.6.1-bin-centos6.tar.gz &&
      	             cd /tmp/thrift-0.6.1 && make install && cd - && rm -rf /tmp/thrift-0.6.1-bin-centos6.tar.gz /tmp/thrift-0.6.1',
      	unless   => '[ -f /usr/local/bin/thrift ]',
      	provider => 'shell',
      	require => [Package['make'], Package['python-devel'], Package['ruby-devel'], Package['automake'], Package['libtool'],
      	            Package['pkgconfig'], Package['gcc-c++'], Package['boost-devel'], Package['libevent-devel'],
      	            Package['zlib-devel'], Package['java-1.7.0-openjdk-devel']]
}


##### RBEnv installation
class { 'rbenv': }
rbenv::plugin { 'sstephenson/ruby-build': }
rbenv::build { '1.9.3-p484': global => true }
